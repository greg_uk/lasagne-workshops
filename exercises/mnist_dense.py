"""
Usage: mnist_dense.py   [options]

Options:
--epochs EPOCHS                 [default: 10]
--batch-size BATCH_SIZE         [default: 32]
--lr LEARNING_RATE              [default: 1.e-3]
"""
import os
import os.path as op

from docopt import docopt

from loaders.mnist import MNIST
from models.mnist_dense import SmallDense
from training.trainers import SGDTrainer
from utils.file_system import create_dir_if_does_not_exist
from utils.visualization import plot_learning_curve

EXERCISE_NAME = "mnist_dense"


def train(epochs, batch_size, learning_rate):
    data_root = os.getenv("DATA_ROOT")
    exercise_dir = op.join(data_root, EXERCISE_NAME)
    create_dir_if_does_not_exist(exercise_dir)

    train_dataset = MNIST("train")
    validation_dataset = MNIST("valid")
    test_dataset = MNIST("test")

    model = SmallDense()

    model_path = op.join(exercise_dir, "{model_name}.pkl".format(model_name=str(model)))
    plot_path = op.join(exercise_dir, "learning_curve_{model_name}.png".format(model_name=str(model)))

    mnist_trainer = SGDTrainer(model, epochs, batch_size, learning_rate)
    train_acc, _, val_acc, _ = mnist_trainer.train(train_dataset, validation_dataset)

    plot_learning_curve(1. - train_acc, 1. - val_acc, plot_path)

    model.save_params(model_path)

    model.load_params(model_path)

    test_loss, test_accuracy = mnist_trainer.test(test_dataset)
    print "Test accuracy: {accuracy}, Test loss: {loss}".format(accuracy=test_accuracy, loss=test_loss)


def main():
    args = docopt(__doc__)
    epochs = int(args["--epochs"])
    batch_size = int(args["--batch-size"])
    learning_rate = float(args["--lr"])
    train(epochs, batch_size, learning_rate)


if __name__ == "__main__":
    main()
