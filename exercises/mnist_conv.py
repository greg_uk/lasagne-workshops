"""
Usage: mnist_dense.py   [options]

Options:
--epochs EPOCHS                 [default: 10]
--batch-size BATCH_SIZE         [default: 32]
--lr LEARNING_RATE              [default: 1.e-3]
--type TYPE                     [default: simple]
"""
import os
import os.path as op

from docopt import docopt

from loaders.mnist import MNIST
from models.mnist_conv import SimpleConv, DeepConv, WideDeepConv
from training.trainers import SGDTrainer
from utils.file_system import create_dir_if_does_not_exist

EXERCISE_NAME = "mnist_conv"

NETS_TYPES = {
    "simple": SimpleConv,
    "deep": DeepConv,
    "wide_deep": WideDeepConv
}


def transformer(input_batch):
    input_batch = input_batch.reshape((input_batch.shape[0], 1, 28, 28))
    return input_batch


def train(epochs, batch_size, learning_rate, type):
    data_root = os.getenv("DATA_ROOT")
    exercise_dir = op.join(data_root, EXERCISE_NAME)
    create_dir_if_does_not_exist(exercise_dir)

    train_dataset = MNIST("train")
    validation_dataset = MNIST("valid")
    test_dataset = MNIST("test")

    model = NETS_TYPES[type]()

    model_path = op.join(exercise_dir, "{model_name}.pkl".format(model_name=str(model)))

    mnist_trainer = SGDTrainer(model, epochs, batch_size, learning_rate, input_type="conv", transformer=transformer)
    mnist_trainer.train(train_dataset, validation_dataset)

    model.save_params(model_path)

    model.load_params(model_path)

    test_loss, test_accuracy = mnist_trainer.test(test_dataset)
    print "Test accuracy: {accuracy}, Test loss: {loss}".format(accuracy=test_accuracy, loss=test_loss)


def main():
    args = docopt(__doc__)
    epochs = int(args["--epochs"])
    batch_size = int(args["--batch-size"])
    learning_rate = float(args["--lr"])
    type = args["--type"]
    train(epochs, batch_size, learning_rate, type)


if __name__ == "__main__":
    main()
