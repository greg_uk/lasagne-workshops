from collections import OrderedDict

from lasagne.layers import InputLayer, DenseLayer
from lasagne.nonlinearities import softmax

from models import Model


class SmallDense(Model):
    def __init__(self):
        self.net = OrderedDict()
        self.net["input"] = InputLayer((None, 784))
        self.net["fc1"] = DenseLayer(self.net["input"], 512)
        self.net["fc2"] = DenseLayer(self.net["fc1"], 256)
        self.net["output"] = DenseLayer(self.net["fc2"], 10, nonlinearity=softmax)
