import os

import cv2
import numpy as np
import theano.tensor as T
import theano
import scipy
from lasagne.layers import get_output
from lasagne.utils import floatX

import vgg19


class ArtImageGenerator(object):

    def __init__(self, vgg19_params_path, iter_num):
        self.image_w = 600
        self.vgg_net = vgg19.load_pretrained_vgg19(vgg19_params_path, self.image_w)
        self.mean = np.array([104, 117, 123], dtype=np.float32).reshape((3, 1, 1))
        self.layer_names = ['conv4_2', 'conv1_1', 'conv2_1', 'conv3_1', 'conv4_1', 'conv5_1']
        self.layers = {k: self.vgg_net[k] for k in self.layer_names}
        self.get_features_func = self.__get_features_func()
        self.iter_num = iter_num

    def generate_image(self, style_im_path, content_im_path, out_dir):
        style_img = self.__preprocess_image(cv2.imread(style_im_path))
        content_img = self.__preprocess_image(cv2.imread(content_im_path))
        style_features = self.__get_features(style_img)
        content_features = self.__get_features(content_img)
        print("ArtImageGenerator: Features for style and content images computed")
        generated_images = self.optimize(style_features, content_features)
        self.save_images(generated_images, out_dir)

    def optimize(self, style_features, content_features):
        def eval_loss(x0):
            x0 = floatX(x0.reshape((1, 3, self.image_w, self.image_w)))
            gen_image.set_value(x0)
            return f_loss().astype(np.float64)

        def eval_grad(x0):
            x0 = floatX(x0.reshape((1, 3, self.image_w, self.image_w)))
            gen_image.set_value(x0)
            return np.array(f_grad()).flatten().astype(np.float64)

        gen_image = theano.shared(floatX(np.random.uniform(-128, 128, (1, 3, self.image_w, self.image_w))))
        gen_features = get_output(self.layers.values(), gen_image)
        gen_features = {k: v for k, v in zip(self.layers.keys(), gen_features)}

        total_loss = self.total_loss(gen_image, gen_features, style_features, content_features)
        f_loss = theano.function([], total_loss)
        f_grad = theano.function([], T.grad(total_loss, gen_image))

        current_img = gen_image.get_value().astype(np.float64)
        gen_images = [current_img]

        print("ArtImageGenerator: Starting optimization for {iter_num} iterations".format(iter_num=self.iter_num))
        for i in xrange(self.iter_num):
            print("ArtImageGenerator: Starting iteration %d" % i)
            scipy.optimize.fmin_l_bfgs_b(eval_loss, current_img.flatten(), fprime=eval_grad, maxfun=40)
            current_img = gen_image.get_value().astype(np.float64)
            gen_images.append(current_img)

        return gen_images

    def total_loss(self, gen_image, gen_features, style_features, content_features):
        losses = [0.001 * self.__content_loss(content_features, gen_features, 'conv4_2')]
        for layer in ['conv1_1', 'conv2_1', 'conv3_1', 'conv4_1', 'conv5_1']:
            losses.append(0.2e6 * self.__style_loss(style_features, gen_features, layer))
        losses.append(0.1e-7 * self.__total_variation_loss(gen_image))
        return sum(losses)

    def __preprocess_image(self, bgr_img_arr):
        if len(bgr_img_arr.shape) < 3:
            img = bgr_img_arr[:, :, np.newaxis]
            bgr_img_arr = np.repeat(img, 3, axis=2)
        h, w, _ = bgr_img_arr.shape
        if h < w:
            bgr_img_arr = cv2.resize(bgr_img_arr, (self.image_w, int(w * self.image_w / h)))
        else:
            bgr_img_arr = cv2.resize(bgr_img_arr, (int(self.image_w * h / w), self.image_w))
        h, w, _ = bgr_img_arr.shape
        crop_img = bgr_img_arr[h//2-self.image_w//2:h//2+self.image_w//2, w//2-self.image_w//2:w//2+self.image_w//2, :]
        crop_img = np.transpose(crop_img, (2, 0, 1)).astype(np.float32)
        crop_img -= self.mean
        return floatX(crop_img[np.newaxis])

    def __deprocess_image(self, img):
        x = np.copy(img[0])
        x += self.mean
        x = np.transpose(x, (1, 2, 0))
        x = np.clip(x, 0, 255).astype(np.uint8)
        return x

    @staticmethod
    def __gram_matrix(feature_map):
        features_flattened = feature_map.flatten(ndim=3)
        gram_mtx = T.tensordot(features_flattened, features_flattened, axes=([2], [2]))
        return gram_mtx

    @staticmethod
    def __content_loss(features_dict1, features_dict2, layer):
        f1, f2 = features_dict1[layer], features_dict2[layer]
        return (1./2) * ((f1 - f2)**2).sum()

    def __style_loss(self, features_dict1, features_dict2, layer):
        f1, f2 = features_dict1[layer], features_dict2[layer]
        gram_mtx1, gram_mtx2 = self.__gram_matrix(f1), self.__gram_matrix(f2)
        n, m = f1.shape[1], f1.shape[2] * f1.shape[3]

        loss = (1./(4 * n*n * m*m)) * ((gram_mtx1 - gram_mtx2) ** 2).sum()
        return loss

    @staticmethod
    def __total_variation_loss(x):
        return (((x[:, :, :-1, :-1] - x[:, :, 1:, :-1]) ** 2 +
                 (x[:, :, :-1, :-1] - x[:, :, :-1, 1:]) ** 2) ** 1.25).sum()

    def __get_features_func(self):
        input_tensor = T.ftensor4()
        out = get_output(self.layers.values(), input_tensor)
        return theano.function([input_tensor], out)

    def __get_features(self, img):
        features = self.get_features_func(img)
        return dict([(k, theano.shared(v)) for k, v in zip(self.layers.keys(), features)])

    def save_images(self, generated_images, out_dir):
        for i, img in enumerate(generated_images):
            im_path = os.path.join(out_dir, "im_%d.jpg" % i)
            cv2.imwrite(im_path, self.__deprocess_image(img))


