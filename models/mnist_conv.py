from collections import OrderedDict

from lasagne.layers import InputLayer, Conv2DLayer, MaxPool2DLayer, DenseLayer
from lasagne.nonlinearities import softmax

from models import Model


class SimpleConv(Model):
    def __init__(self):
        self.net = OrderedDict()
        self.net["input"] = InputLayer((None, 1, 28, 28))
        self.net["conv1"] = Conv2DLayer(self.net["input"], 32, 6)
        self.net["conv2"] = Conv2DLayer(self.net["conv1"], 32, 6)
        self.net["pool"] = MaxPool2DLayer(self.net["conv2"], 2, stride=2)
        self.net["fc1"] = DenseLayer(self.net["pool"], 1024)
        self.net["output"] = DenseLayer(self.net["fc1"], 10, nonlinearity=softmax)


class DeepConv(Model):
    def __init__(self):
        self.net = OrderedDict()
        self.net["input"] = InputLayer((None, 1, 28, 28))
        self.net["conv1"] = Conv2DLayer(self.net["input"], 32, 3, stride=1, pad=1)
        self.net["conv2"] = Conv2DLayer(self.net["conv1"], 64, 3, stride=1, pad=1)
        self.net["pool1"] = MaxPool2DLayer(self.net["conv2"], 2, stride=2)
        self.net["conv3"] = Conv2DLayer(self.net["pool1"], 64, 3, stride=1, pad=1)
        self.net["conv4"] = Conv2DLayer(self.net["conv3"], 128, 3, stride=1, pad=1)
        self.net["pool2"] = MaxPool2DLayer(self.net["conv4"], 2, stride=2)
        self.net["conv5"] = Conv2DLayer(self.net["pool2"], 128, 3, stride=1, pad=1)
        self.net["conv6"] = Conv2DLayer(self.net["conv5"], 256, 3, stride=1, pad=1)
        self.net["pool3"] = MaxPool2DLayer(self.net["conv6"], 4, stride=4)
        self.net["fc1"] = DenseLayer(self.net["pool3"], 256)
        self.net["output"] = DenseLayer(self.net["fc1"], 10, nonlinearity=softmax)


class WideDeepConv(Model):
    def __init__(self):
        self.net = OrderedDict()
        self.net["input"] = InputLayer((None, 1, 28, 28))
        self.net["conv1"] = Conv2DLayer(self.net["input"], 256, 5, stride=1, pad=1)
        self.net["conv2"] = Conv2DLayer(self.net["conv1"], 256, 5, stride=1, pad=1)
        self.net["pool1"] = MaxPool2DLayer(self.net["conv2"], 2, stride=2)
        self.net["conv3"] = Conv2DLayer(self.net["pool1"], 512, 3, stride=1, pad=1)
        self.net["conv4"] = Conv2DLayer(self.net["conv3"], 512, 3, stride=1, pad=1)
        self.net["pool2"] = MaxPool2DLayer(self.net["conv4"], 2, stride=2)
        self.net["conv5"] = Conv2DLayer(self.net["pool2"], 1024, 3, stride=1, pad=1)
        self.net["conv6"] = Conv2DLayer(self.net["conv5"], 1024, 3, stride=1, pad=1)
        self.net["pool3"] = MaxPool2DLayer(self.net["conv6"], 2, stride=2)
        self.net["fc1"] = DenseLayer(self.net["pool3"], 256)
        self.net["output"] = DenseLayer(self.net["fc1"], 10, nonlinearity=softmax)
